yaml = require('js-yaml');
fs   = require('fs');
const curl = new (require( 'curl-request' ))();

try {
  var doc = yaml.safeLoad(fs.readFileSync('config.yml', 'utf8'));
  var theme_id = doc.development.theme_id;
  var username = doc.development.username;
  var password = doc.development.password
  var store = doc.development.store;
  var name = process.argv.slice(2)[0];

  var url = 'https://' + username + ':' + password + '@' + store + '/admin/api/2019-07/themes/' + theme_id + '.json';
  var data = '{ "theme": { "id": ' + theme_id + ', "role": "main", "name" : "' + name + '" } }';

  curl.setHeaders([
    'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
    'Content-Type: application/json'
  ]).setBody(data).patch(url)
    .then(({statusCode, body, headers}) => {
      console.log(statusCode, body)
    })
    .catch((e) => {
      console.log(e);
  });
} catch (e) {
  console.log(e);
}
